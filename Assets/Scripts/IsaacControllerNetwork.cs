﻿/*
* Copyright (c) AtheroX
* https://twitter.com/athero_x
*/

using Mirror;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class IsaacControllerNetwork : NetworkBehaviour {

	//public void SetColor(Color playerColor) => this.playerColor = playerColor;
	Color[] colores = { Color.red, Color.blue, Color.yellow, Color.green, Color.cyan, Color.magenta, Color.white, Color.black};
	static int micolor = 0;
    public int NumPlayers  {
        set {
            numPlayers = value;
			Color colorsito = colores[micolor % colores.Length];
            //if(numPlayers == 1) colorsito = Color.red;
            //else if(numPlayers == 2) colorsito = Color.blue;
            //else if(numPlayers == 3) colorsito = Color.green;
            playerColor = colorsito;
			micolor++;
        }
        get { return numPlayers; }
    }
    int numPlayers;

    [SyncVar(hook = nameof(Colorize))]
    [SerializeField] Color playerColor = Color.white;

    void Colorize(Color inutil , Color newColor) {
        playerColor = newColor;
        transform.GetChild(0).GetComponent<SpriteRenderer>().color = transform.GetChild(1).GetComponent<SpriteRenderer>().color = playerColor;
    }


    [SerializeField] private Animator cabesaAnim;
	[SerializeField] private Animator patasAnim;
	[SerializeField] private Transform shootPos;
	[SerializeField] private Rigidbody2D rb;

	private float moveX, moveY;
	private bool canShoot = true;

	public float speed = 5f;
	public float tearSpeed = 100f;
	public float tearCd = 0.5f;
	public float dmg = 3.5f;
	public GameObject tear;



    public override void OnStartLocalPlayer() {
		base.OnStartLocalPlayer();

        transform.GetChild(0).TryGetComponent<Animator>(out cabesaAnim);
		shootPos = transform.GetChild(0).GetChild(0);
		transform.GetChild(1).TryGetComponent<Animator>(out patasAnim);

		TryGetComponent<Rigidbody2D>(out rb);

	}

	public override void OnStopClient() {
		base.OnStopClient();
		print("adios");
	}



	void Update() {
		if (!isLocalPlayer) return;

		moveX = Input.GetAxis("Horizontal");
		moveY = Input.GetAxis("Vertical");

		if (cabesaAnim != null) {
			cabesaAnim.SetFloat("MoveX", moveX);
			cabesaAnim.SetFloat("MoveY", moveY);
		}
		if (patasAnim != null) {
			patasAnim.SetFloat("MoveX", moveX);
			patasAnim.SetFloat("MoveY", moveY);
		}

		if (Input.GetButton("ShootHorizontal") || Input.GetButton("ShootVertical"))
			Shoot();
	}

	void FixedUpdate() {
		if (!isLocalPlayer) return;
		rb.velocity = new Vector2(moveX, moveY) * speed;
		if (moveX != 0) {
			if (patasAnim != null) {
				if (moveX < 0)
					transform.GetChild(1).localScale = new Vector3(-1, 1, 1);
				else
					transform.GetChild(1).localScale = new Vector3(1, 1, 1);
			}
		}
	}

	[Client]
	private void Shoot() {
		//print("disparo");
		float horiz = Input.GetAxisRaw("ShootHorizontal");
		float vertical = Input.GetAxisRaw("ShootVertical");

		cabesaAnim.SetFloat("MoveX", horiz);
		cabesaAnim.SetFloat("MoveY", vertical);

		if (canShoot) {
			canShoot = false;

			if (cabesaAnim != null)
				cabesaAnim.SetBool("Shoot", true);

			if (Mathf.Abs(horiz) > Mathf.Abs(vertical)) {
				CommandShoot(horiz, vertical, Vector2.right, horiz);
				if (cabesaAnim != null) {
					cabesaAnim.SetFloat("MoveX", horiz);
					cabesaAnim.SetFloat("MoveY", 0);
				}

			} else if (Mathf.Abs(horiz) < Mathf.Abs(vertical)) {
				CommandShoot(horiz, vertical, Vector2.up, vertical);
				if (cabesaAnim != null) {
					cabesaAnim.SetFloat("MoveX", 0);
					cabesaAnim.SetFloat("MoveY", vertical);
				}
			} else {
				if (Mathf.Abs(horiz) != 0) {
					CommandShoot(horiz, vertical, Vector2.right, horiz);
					if (cabesaAnim != null) {
						cabesaAnim.SetFloat("MoveX", horiz);
						cabesaAnim.SetFloat("MoveY", 0);
					}
				} else if (Mathf.Abs(vertical) != 0) {
					CommandShoot(horiz, vertical, Vector2.up, vertical);
					if (cabesaAnim != null) {
						cabesaAnim.SetFloat("MoveX", 0);
						cabesaAnim.SetFloat("MoveY", vertical);
					}

				} else {
					if (Input.GetButton("ShootHorizontal")) {
						CommandShoot(horiz, vertical, Vector2.right, 1);
						if (cabesaAnim != null) {
							cabesaAnim.SetFloat("MoveX", 1);
							cabesaAnim.SetFloat("MoveY", 0);
						}

					} else {
						CommandShoot(horiz, vertical, Vector2.up, -1);
						if (cabesaAnim != null) {
							cabesaAnim.SetFloat("MoveX", 0);
							cabesaAnim.SetFloat("MoveY", -1);
						}
					}
				}
			}
			Invoke(nameof(CanShootAgain), tearCd);
		}
	}

	[Command]
	private void CommandShoot(float horiz, float vertical, Vector2 dir, float sentido) {

		GameObject newBullet = Instantiate(tear, transform);
		newBullet.GetComponent<Tears>().Dmg = dmg;
		Rigidbody2D bullRb = newBullet.GetComponent<Rigidbody2D>();

		newBullet.transform.parent = transform.parent;
		newBullet.transform.position = bullRb.position = shootPos.position+(new Vector3(dir.x*sentido,dir.y*sentido)*0.3f);

		bullRb.velocity = dir * tearSpeed * Time.fixedDeltaTime * sentido + (rb.velocity / 2);

		Destroy(newBullet, 2f);

		NetworkServer.Spawn(newBullet);
	}

	private void CanShootAgain() {
		canShoot = true;
		if (cabesaAnim != null)
			cabesaAnim.SetBool("Shoot", false);
	}

	//TODO
	IEnumerator DamageEffectSequence(Color sr, Color dmgColor, Color originalColor, float duration) {
		sr = dmgColor;
		for (float t = 0; t < 1.0f; t += Time.deltaTime / duration) {
			sr = Color.Lerp(dmgColor, originalColor, t);
			yield return null;
		}
		sr = originalColor;
	}

}
